package com.movement.beadedjwellery.design.ideas

class CustomException(message: String) : Exception(message)
