package com.movement.beadedjwellery.design.ideas


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_start_screen.view.*


class StartScreenFragment : androidx.fragment.app.Fragment() {
    lateinit var act:AppInterfaces

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the rate_me_layout for this fragment
//        val v = inflater.inflate(R.layout.fragment_start_screen, container, false)
        val startScreen = inflater.inflate(R.layout.fragment_start_screen, container, false)

        startScreen.btnGalleryLayout03.setOnClickListener {
            AdObject.admob?.loadNextScreen { act.loadImageTopics() }
        }
        startScreen.btnGallery03.setOnClickListener {
            AdObject.admob?.loadNextScreen { act.loadImageTopics() }
        }
        startScreen.imgBtnRateMe03.setOnClickListener {
            AppUtils().rateApp(context as Context)
        }
        startScreen.imgBtnShare03.setOnClickListener(View.OnClickListener {
            AppUtils().shareApp(context as Context)
        })

        startScreen.tvPrivacy03.setOnClickListener {
            AdObject.admob?.loadNextScreen{act.loadPrivacyPolicy()}
        }


        return startScreen
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AppInterfaces){
            act = context
        }
    }
}
